package startit.filter;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<Employee>();
        employees.add(new Employee("John", 25));
        employees.add(new Employee("Mary", 27));
        employees.add(new Employee("Eric", 21));

        Employee employee = employees.stream().filter(e -> {
            if (25 == e.getAge()) {

                System.out.println("Employee found!");
                return true;
            }

            return false;
        })
                .findAny()
                .orElse(null);

        System.out.println(employee);

    }


}
